﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ModderBrothers___RomMerger
{
  public partial class wRomsMerger : Form
  {
    const string EmptySlot = "[none]";

    private bool IsPowerOfTwo(int value)
    {
      for (int i = 32; --i >= 0; value >>= 1)
        if ((value & 1) != 0)
          return (value >> 1) == 0;
      return true; // zero is aligned
    }

    private byte[] DuplicateRom(byte[] rom)
    {
      byte[] result = new byte[rom.Length * 2];
      rom.CopyTo(result, 0);
      rom.CopyTo(result, rom.Length);
      return result;
    }

    private int NextPowerOfTwo(int size)
    {
      for (int i = 32; --i >= 0;)
        if ((size & (1 << i)) != 0)
          return 1 << (i + 1);
      return 0;
    }

    private byte[] AlignRom(byte[] rom)
    {
      if (!IsPowerOfTwo(rom.Length))
      {
        int size = NextPowerOfTwo(rom.Length);
        byte[] result = new byte[size];
        rom.CopyTo(result, 0);
        return result;
      }
      return rom;
    }

    private byte[] BuildSizedRom(string path)
    {
      try
      {
        byte[] rom = File.ReadAllBytes(path);
        if (rom.Length != 0) // Do not process empty files
        {
          rom = AlignRom(rom);
          while (rom.Length < MaxSizeForCurrentCartridge())
            rom = DuplicateRom(rom);
        }
        return rom;
      }
      catch(Exception ex)
      {
        MessageBox.Show(string.Format("Error loading file {0}!\n\n{1}", path, ex.Message), "File error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return new byte[0];
      }
    }

    private void PrepareListView()
    {
      lvFiles.Items.Clear();
      for (int i = 0; ++i <= FilesInCurrentCartridge();)
      {
        ListViewItem item = new ListViewItem();
        item.Text = i.ToString();
        item.SubItems.Add(EmptySlot);
        item.SubItems.Add("-");
        item.SubItems.Add("");
        lvFiles.Items.Add(item);
      }
      lvFiles.Columns[1].Width = lvFiles.ClientSize.Width - lvFiles.Columns[0].Width - lvFiles.Columns[2].Width;
      lvFiles.Items[0].Selected = true;
      ForceShowSelection();
      UpdateCount();
    }

    private void MoveItems(int from, int delta)
    {
      ListViewItem source = (ListViewItem)lvFiles.Items[from].Clone();
      ListViewItem destination = (ListViewItem)lvFiles.Items[from + delta].Clone();
      string indexSource = source.Text;
      string indexDestination = destination.Text;
      source.Text = source.SubItems[0].Text = indexDestination;
      destination.Text = destination.SubItems[0].Text = indexSource;
      lvFiles.Items[from] = destination;
      lvFiles.Items[from + delta] = source;
      lvFiles.Items[from + delta].Selected = true;
      ForceShowSelection();
    }

    private int SlotCount()
    {
      int count = 0;
      foreach (ListViewItem item in lvFiles.Items)
        if (!IsEmptySlot(item))
          count++;
      return count;
    }

    private void UpdateCount()
    {
      lbCount.Text = string.Format("{0}/{1}", SlotCount(), FilesInCurrentCartridge());
    }

    private void ForceShowSelection()
    {
      lvFiles.HideSelection = false;
      if (lvFiles.SelectedIndices.Count != 0)
        lvFiles.EnsureVisible(lvFiles.SelectedIndices[0]);
      lvFiles.Select();
    }

    private bool HasFiles()
    {
      bool found = false;
      foreach (ListViewItem item in lvFiles.Items)
        if (!IsEmptySlot(item))
          found = true;
      return found;
    }

    private bool HasRoom()
    {
      bool found = false;
      foreach (ListViewItem item in lvFiles.Items)
        if (IsEmptySlot(item))
          found = true;
      return found;
    }

    private bool IsSelectedSlotEmpty()
    {
      return (lvFiles.SelectedItems[0].SubItems[1].Text == EmptySlot);
    }

    private bool IsEmptySlot(int slot)
    {
      return (lvFiles.Items[slot].SubItems[1].Text == EmptySlot);
    }

    private bool IsEmptySlot(ListViewItem item)
    {
      return (item.SubItems[1].Text == EmptySlot);
    }

    public void UpdateButtonState()
    {
      btAdd.Enabled = HasRoom();
      btRemove.Enabled = !IsSelectedSlotEmpty();
      btMoveUp.Enabled = btRemove.Enabled && lvFiles.SelectedIndices.Count != 0 && lvFiles.SelectedIndices[0] > 0;
      btMoveDown.Enabled = btRemove.Enabled && lvFiles.SelectedIndices.Count != 0 && lvFiles.SelectedIndices[0] < SlotCount() - 1;
      btRemoveAll.Enabled = btSave.Enabled = HasFiles();
      UpdateCount();
    }

    public int FilesPerCartridge(int cartridge)
    {
      switch(cartridge)
      {
        case 0: // Atari 2600 16 games
        case 1: // Coleco 16 games
        case 2: // EXL100 16 games
          return 16;
        default: break;
      }
      throw new IndexOutOfRangeException("Invalid cartridge index!");
    }

    public int FilesInCurrentCartridge()
    {
      return FilesPerCartridge(cbCartridge.SelectedIndex);
    }

    public int MaxSizePerCartridge(int cartridge)
    {
      switch (cartridge)
      {
        case 0: // Atari 2600 16 games
        case 1: // Coleco 16 games
        case 2: // EXL100 16 games
          return 32 << 10;
        default: break;
      }
      throw new IndexOutOfRangeException("Invalid cartridge index!");
    }

    public int MaxSizeForCurrentCartridge()
    {
      return MaxSizePerCartridge(cbCartridge.SelectedIndex);
    }

    public string FileFiltersPerCartridge(int cartridge)
    {
      switch (cartridge)
      {
        case 0: return "Atari 2600 roms (*.a26;*.bin)|*.a26;*.bin|All files (*.*)|*.*"; // Atari 2600 16 games
        case 1: return "Colecovision roms (*.col;*.bin)|*.col;*.bin|All files (*.*)|*.*"; // Coleco 16 games
        case 2: return "EXL100 (*.rom;*.bin)|*.rom;*.bin|All files (*.*)|*.*"; // EXL100 16 games
        default: break;
      }
      throw new IndexOutOfRangeException("Invalid cartridge index!");
    }

    public string FileFiltersForCurrentCartridge()
    {
      return FileFiltersPerCartridge(cbCartridge.SelectedIndex);
    }

    // ----------------------------

    public wRomsMerger()
    {
      InitializeComponent();
    }

    private void listView1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (lvFiles.SelectedItems.Count == 0) return;
      UpdateButtonState();
    }

    private void wRomsMerger_Load(object sender, EventArgs e)
    {
      cbCartridge.SelectedIndex = 0;
      PrepareListView();
      UpdateButtonState();
    }

    private void cbCartridge_SelectionChangeCommitted(object sender, EventArgs e)
    {
      if (HasFiles())
        if (MessageBox.Show("Remove all files?", "Change cartridge type", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
          PrepareListView();
    }

    private void btAdd_Click(object sender, EventArgs e)
    {
      ofSelectRom.Filter = FileFiltersForCurrentCartridge();
      if (ofSelectRom.ShowDialog() == DialogResult.OK)
      {
        bool overrun = false;
        bool underrun = false;
        foreach (string filename in ofSelectRom.FileNames)
        {
          int index = 0;
          while (index < FilesInCurrentCartridge() && !IsEmptySlot(index)) ++index;
          if (index >= FilesInCurrentCartridge()) break;
          Int64 size = new FileInfo(filename).Length;
          if (size > MaxSizeForCurrentCartridge()) overrun = true;
          if (size == 0) underrun = true;
          lvFiles.Items[index].SubItems[1].Text = Path.GetFileName(filename);
          lvFiles.Items[index].SubItems[2].Text = (size >> 10).ToString() + "KB";
          lvFiles.Items[index].SubItems[3].Text = Path.GetDirectoryName(filename);
          lvFiles.Items[index].Selected = true;
        }
        ForceShowSelection();
        UpdateButtonState();
        if (overrun)
          MessageBox.Show(string.Format("Some selected files exceed the limit of {0}KB and have not been addded to the list.", MaxSizeForCurrentCartridge() >> 10), "Files too bigs!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        if (underrun)
          MessageBox.Show("Some selected files are empty and have not been addded to the list.", "Empty files!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }
    }

    private void btRemove_Click(object sender, EventArgs e)
    {
      if (!IsSelectedSlotEmpty())
      {
        lvFiles.SelectedItems[0].SubItems[1].Text = EmptySlot;
        lvFiles.SelectedItems[0].SubItems[2].Text = "-";
        lvFiles.SelectedItems[0].SubItems[3].Text = "";
        UpdateButtonState();
      }
    }

    private void btRemoveAll_Click(object sender, EventArgs e)
    {
      PrepareListView();
      UpdateButtonState();
    }

    private void btMoveUp_Click(object sender, EventArgs e)
    {
      if (!IsSelectedSlotEmpty() && lvFiles.SelectedIndices[0] > 0)
        MoveItems(lvFiles.SelectedIndices[0], -1);
    }

    private void btMoveDown_Click(object sender, EventArgs e)
    {
      if (!IsSelectedSlotEmpty() && lvFiles.SelectedIndices[0] < SlotCount() - 1)
        MoveItems(lvFiles.SelectedIndices[0], 1);
    }

    private void btSave_Click(object sender, EventArgs e)
    {
      sfSave.FileName = cbCartridge.Text + ".bin";
      List<string> romPerSlots = new List<string>();
      if (sfSave.ShowDialog() == DialogResult.OK)
      {
        MemoryStream output = new MemoryStream();
        int emptySlots = 0;
        int romSlots = 0;
        int wraps = 0;
        int index = 0;
        for (int i = 0; i < FilesInCurrentCartridge(); ++i)
        {
          byte[] rom = null;
          if (!IsEmptySlot(index))
          {
            rom = BuildSizedRom(Path.Combine(lvFiles.Items[index].SubItems[3].Text, lvFiles.Items[index].SubItems[1].Text));
            if (rom.Length != 0) romPerSlots.Add(string.Format("#{0,2} - {1}", i, Path.GetFileNameWithoutExtension(lvFiles.Items[index].SubItems[1].Text)));
          }
          if (rom == null || rom.Length == 0)
          {
            rom = new byte[MaxSizeForCurrentCartridge()];
            romPerSlots.Add(string.Format("#{0:2} - {1}", i, "[No rom here!]"));
            emptySlots++;
          }
          else romSlots++;
          output.Write(rom);
          if (++index >= SlotCount())
            if (cbAutofill.Checked)
            {
              index = 0;
              wraps++;
            }
        }
        try
        {
          File.WriteAllBytes(sfSave.FileName, output.ToArray());
          string reportFilename = Path.ChangeExtension(sfSave.FileName, ".txt");
          File.WriteAllLines(reportFilename, romPerSlots);
          MessageBox.Show(string.Format("Ready-to-flash binary file of {5}KB saved to {0}!\n\n" +
                                        "{1} Slots containing roms\n{2} Empty slots\nRoms repeated up to {3} times.\n\n" +
                                        "A complete text report has been saved to {4}",
                                        sfSave.FileName, romSlots, emptySlots, wraps, reportFilename, output.Length >> 10),
                                        "Rom file saved succesfully!", MessageBoxButtons.OK, MessageBoxIcon.Information);                                
        }
        catch (Exception ex)
        {
          MessageBox.Show("Error saving finale cartridge file!\n\n" + ex.Message, "File error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }
    }
  }
}
