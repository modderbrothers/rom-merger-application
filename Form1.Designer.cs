﻿
namespace ModderBrothers___RomMerger
{
  partial class wRomsMerger
  {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbCartridge = new System.Windows.Forms.ComboBox();
      this.pnTitle = new System.Windows.Forms.Panel();
      this.lbCartridge = new System.Windows.Forms.Label();
      this.lvFiles = new System.Windows.Forms.ListView();
      this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
      this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
      this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
      this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
      this.btAdd = new System.Windows.Forms.Button();
      this.btRemove = new System.Windows.Forms.Button();
      this.btRemoveAll = new System.Windows.Forms.Button();
      this.btSave = new System.Windows.Forms.Button();
      this.btMoveUp = new System.Windows.Forms.Button();
      this.btMoveDown = new System.Windows.Forms.Button();
      this.cbAutofill = new System.Windows.Forms.CheckBox();
      this.ofSelectRom = new System.Windows.Forms.OpenFileDialog();
      this.lbCount = new System.Windows.Forms.Label();
      this.sfSave = new System.Windows.Forms.SaveFileDialog();
      this.SuspendLayout();
      // 
      // cbCartridge
      // 
      this.cbCartridge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbCartridge.FormattingEnabled = true;
      this.cbCartridge.Items.AddRange(new object[] {
            "Atari 2600 16 Games Cartridge",
            "Colecovision 16 Games Cartridge",
            "EXL100 16 Applications/Games Cartridge"});
      this.cbCartridge.Location = new System.Drawing.Point(83, 162);
      this.cbCartridge.Name = "cbCartridge";
      this.cbCartridge.Size = new System.Drawing.Size(272, 23);
      this.cbCartridge.TabIndex = 0;
      this.cbCartridge.SelectionChangeCommitted += new System.EventHandler(this.cbCartridge_SelectionChangeCommitted);
      // 
      // pnTitle
      // 
      this.pnTitle.BackgroundImage = global::ModderBrothers___RomMerger.Properties.Resources.ModderBrothersBlack;
      this.pnTitle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.pnTitle.Location = new System.Drawing.Point(12, 12);
      this.pnTitle.Name = "pnTitle";
      this.pnTitle.Size = new System.Drawing.Size(610, 135);
      this.pnTitle.TabIndex = 1;
      // 
      // lbCartridge
      // 
      this.lbCartridge.AutoSize = true;
      this.lbCartridge.Location = new System.Drawing.Point(12, 165);
      this.lbCartridge.Name = "lbCartridge";
      this.lbCartridge.Size = new System.Drawing.Size(56, 15);
      this.lbCartridge.TabIndex = 2;
      this.lbCartridge.Text = "Cartridge";
      // 
      // lvFiles
      // 
      this.lvFiles.Activation = System.Windows.Forms.ItemActivation.OneClick;
      this.lvFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
      this.lvFiles.FullRowSelect = true;
      this.lvFiles.GridLines = true;
      this.lvFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this.lvFiles.HideSelection = false;
      this.lvFiles.Location = new System.Drawing.Point(12, 200);
      this.lvFiles.MultiSelect = false;
      this.lvFiles.Name = "lvFiles";
      this.lvFiles.Size = new System.Drawing.Size(610, 210);
      this.lvFiles.TabIndex = 3;
      this.lvFiles.UseCompatibleStateImageBehavior = false;
      this.lvFiles.View = System.Windows.Forms.View.Details;
      this.lvFiles.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
      this.lvFiles.DoubleClick += new System.EventHandler(this.btAdd_Click);
      // 
      // columnHeader1
      // 
      this.columnHeader1.Text = "#";
      this.columnHeader1.Width = 40;
      // 
      // columnHeader2
      // 
      this.columnHeader2.Text = "Rom File";
      this.columnHeader2.Width = 466;
      // 
      // columnHeader3
      // 
      this.columnHeader3.Text = "Size";
      this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.columnHeader3.Width = 100;
      // 
      // columnHeader4
      // 
      this.columnHeader4.Text = "Path";
      this.columnHeader4.Width = 0;
      // 
      // btAdd
      // 
      this.btAdd.Location = new System.Drawing.Point(12, 419);
      this.btAdd.Name = "btAdd";
      this.btAdd.Size = new System.Drawing.Size(75, 23);
      this.btAdd.TabIndex = 4;
      this.btAdd.Text = "Add";
      this.btAdd.UseVisualStyleBackColor = true;
      this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
      // 
      // btRemove
      // 
      this.btRemove.Location = new System.Drawing.Point(93, 419);
      this.btRemove.Name = "btRemove";
      this.btRemove.Size = new System.Drawing.Size(75, 23);
      this.btRemove.TabIndex = 5;
      this.btRemove.Text = "Remove";
      this.btRemove.UseVisualStyleBackColor = true;
      this.btRemove.Click += new System.EventHandler(this.btRemove_Click);
      // 
      // btRemoveAll
      // 
      this.btRemoveAll.Location = new System.Drawing.Point(174, 419);
      this.btRemoveAll.Name = "btRemoveAll";
      this.btRemoveAll.Size = new System.Drawing.Size(75, 23);
      this.btRemoveAll.TabIndex = 6;
      this.btRemoveAll.Text = "Remove All";
      this.btRemoveAll.UseVisualStyleBackColor = true;
      this.btRemoveAll.Click += new System.EventHandler(this.btRemoveAll_Click);
      // 
      // btSave
      // 
      this.btSave.Location = new System.Drawing.Point(547, 419);
      this.btSave.Name = "btSave";
      this.btSave.Size = new System.Drawing.Size(75, 23);
      this.btSave.TabIndex = 7;
      this.btSave.Text = "Save";
      this.btSave.UseVisualStyleBackColor = true;
      this.btSave.Click += new System.EventHandler(this.btSave_Click);
      // 
      // btMoveUp
      // 
      this.btMoveUp.Location = new System.Drawing.Point(306, 419);
      this.btMoveUp.Name = "btMoveUp";
      this.btMoveUp.Size = new System.Drawing.Size(75, 23);
      this.btMoveUp.TabIndex = 8;
      this.btMoveUp.Text = "Move up";
      this.btMoveUp.UseVisualStyleBackColor = true;
      this.btMoveUp.Click += new System.EventHandler(this.btMoveUp_Click);
      // 
      // btMoveDown
      // 
      this.btMoveDown.Location = new System.Drawing.Point(387, 419);
      this.btMoveDown.Name = "btMoveDown";
      this.btMoveDown.Size = new System.Drawing.Size(83, 23);
      this.btMoveDown.TabIndex = 9;
      this.btMoveDown.Text = "Move down";
      this.btMoveDown.UseVisualStyleBackColor = true;
      this.btMoveDown.Click += new System.EventHandler(this.btMoveDown_Click);
      // 
      // cbAutofill
      // 
      this.cbAutofill.AutoSize = true;
      this.cbAutofill.Checked = true;
      this.cbAutofill.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbAutofill.Location = new System.Drawing.Point(488, 164);
      this.cbAutofill.Name = "cbAutofill";
      this.cbAutofill.Size = new System.Drawing.Size(134, 19);
      this.cbAutofill.TabIndex = 10;
      this.cbAutofill.Text = "Auto-fill empty slots";
      this.cbAutofill.UseVisualStyleBackColor = true;
      // 
      // ofSelectRom
      // 
      this.ofSelectRom.Multiselect = true;
      this.ofSelectRom.Title = "Select rom file(s)";
      // 
      // lbCount
      // 
      this.lbCount.AutoSize = true;
      this.lbCount.Location = new System.Drawing.Point(405, 165);
      this.lbCount.Name = "lbCount";
      this.lbCount.Size = new System.Drawing.Size(24, 15);
      this.lbCount.TabIndex = 11;
      this.lbCount.Text = "0/0";
      // 
      // sfSave
      // 
      this.sfSave.DefaultExt = "bin";
      this.sfSave.Filter = "Binary files (*.bin)|*.bin";
      this.sfSave.RestoreDirectory = true;
      // 
      // wRomsMerger
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(634, 454);
      this.Controls.Add(this.lbCount);
      this.Controls.Add(this.cbAutofill);
      this.Controls.Add(this.btMoveDown);
      this.Controls.Add(this.btMoveUp);
      this.Controls.Add(this.btSave);
      this.Controls.Add(this.btRemoveAll);
      this.Controls.Add(this.btRemove);
      this.Controls.Add(this.btAdd);
      this.Controls.Add(this.lvFiles);
      this.Controls.Add(this.lbCartridge);
      this.Controls.Add(this.pnTitle);
      this.Controls.Add(this.cbCartridge);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Name = "wRomsMerger";
      this.Text = "ModderBrothers - Roms Merger v1.0";
      this.Load += new System.EventHandler(this.wRomsMerger_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox cbCartridge;
    private System.Windows.Forms.Panel pnTitle;
    private System.Windows.Forms.Label lbCartridge;
    private System.Windows.Forms.ListView lvFiles;
    private System.Windows.Forms.ColumnHeader columnHeader1;
    private System.Windows.Forms.ColumnHeader columnHeader2;
    private System.Windows.Forms.ColumnHeader columnHeader3;
    private System.Windows.Forms.Button btAdd;
    private System.Windows.Forms.Button btRemove;
    private System.Windows.Forms.Button btRemoveAll;
    private System.Windows.Forms.Button btSave;
    private System.Windows.Forms.Button btMoveUp;
    private System.Windows.Forms.Button btMoveDown;
    private System.Windows.Forms.CheckBox cbAutofill;
    private System.Windows.Forms.ColumnHeader columnHeader4;
    private System.Windows.Forms.OpenFileDialog ofSelectRom;
    private System.Windows.Forms.Label lbCount;
    private System.Windows.Forms.SaveFileDialog sfSave;
  }
}

