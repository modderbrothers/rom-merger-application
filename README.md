# ModderBrothers' Rom Merger Application

Rom merger application is a small .Net tool to resize & aggregate multiple roms into a single rom file ready to be flashed to ModderBrothers' games cartridges.

Developped in Winform, it runs only on Windows for now. Will be subject to change in a near future to support Linux & OSX

Supports currently:
- Atari 2600 16 games cartridges (8k per game max)
- Colecovision 16 games cartridges
- EXL100 16 games cartridges